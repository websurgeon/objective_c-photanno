//
//  MessageVC.h
//  PhotAnno
//
//  Created by Peter on 13/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *messageView; // the view that is shown/hidden. self.view is just a transparent container
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@property (strong, nonatomic) NSString *message;
@property (nonatomic, readonly) BOOL isVisible;

- (void)showMessage:(NSString *)message forDuration:(NSTimeInterval)duration;
- (void)showMessage:(NSString *)message;
- (void)showForDuration:(NSTimeInterval)duration;
- (void)showAnimated:(BOOL)animated;
- (void)hideAnimated:(BOOL)animated;
- (void)show;
- (void)hide;

@end
