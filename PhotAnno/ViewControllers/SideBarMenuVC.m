//
//  SideBarMenuVC.m
//  PhotAnno
//
//  Created by Pete on 19/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "SideBarMenuVC.h"
#import <QuartzCore/QuartzCore.h>

@interface SideBarMenuVC ()

@end

@implementation SideBarMenuVC

@synthesize btnEditToggle = _btnEditToggle;

@synthesize delegate = _delegate;
@synthesize menuView = _menuView;

- (BOOL)isVisible
{
    return ![self.menuView isHidden];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self.view setClipsToBounds:YES];
    [self.view setBackgroundColor:[UIColor clearColor]];
    [self.view setOpaque:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closePhoto:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(sideBarMenu:didTapMenuItem:)]) {
        [self.delegate sideBarMenu:self didTapMenuItem:sender];
    }
    if ([self.delegate respondsToSelector:@selector(didTapCloseOnSideBarMenu:)]) {
        [self.delegate didTapCloseOnSideBarMenu:self];
    }
}

- (IBAction)save:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(sideBarMenu:didTapMenuItem:)]) {
        [self.delegate sideBarMenu:self didTapMenuItem:sender];
    }
    if ([self.delegate respondsToSelector:@selector(didTapSaveOnSideBarMenu:)]) {
        [self.delegate didTapSaveOnSideBarMenu:self];
    }
}

- (IBAction)toggleEditMode:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(sideBarMenu:didTapMenuItem:)]) {
        [self.delegate sideBarMenu:self didTapMenuItem:sender];
    }
    if ([self.delegate respondsToSelector:@selector(didTapToggleEditModeOnSideBarMenu:)]) {
        [self.delegate didTapToggleEditModeOnSideBarMenu:self];
    }
}

- (void)showAnimated:(BOOL)animated
{
    NSTimeInterval duration =(animated ? 0.5 : 0);
    if ([self.delegate respondsToSelector:@selector(sideBarMenu:willShowWithDuration:)]) {
        [self.delegate sideBarMenu:self willShowWithDuration:duration];
    }

    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hide) object:nil];
    if (animated && [self.menuView  isHidden]) {
        // when menu view is initialised it may not be in the correct location
        // so we need to ensure that if the menuView is hidden that the animation will start from the away position
        [self moveMenuViewAway];
    }
    [self.menuView setHidden:NO];
    if (animated) {
        [UIView animateWithDuration:duration animations:^{
            [self moveMenuViewBack];
        } completion:^(BOOL finished) {
            if (finished) {
                if ([self.delegate respondsToSelector:@selector(sideBarMenu:didShowAnimated:)]) {
                    [self.delegate sideBarMenu:self didShowAnimated:animated];
                }
            }
        }];
    } else {
        [self moveMenuViewBack];
        if ([self.delegate respondsToSelector:@selector(sideBarMenu:didShowAnimated:)]) {
            [self.delegate sideBarMenu:self didShowAnimated:animated];
        }
    }
}

- (void)hideAnimated:(BOOL)animated
{
    NSTimeInterval duration =(animated ? 0.5 : 0);
    if ([self.delegate respondsToSelector:@selector(sideBarMenu:willHideWithDuration:)]) {
        [self.delegate sideBarMenu:self willHideWithDuration:duration];
    }
    if (animated) {
        [UIView animateWithDuration:duration animations:^{
            [self moveMenuViewAway];
        } completion:^(BOOL finished) {
            if (finished) {
                [self.menuView setHidden:YES];
                if ([self.delegate respondsToSelector:@selector(sideBarMenu:didHideAnimated:)]) {
                    [self.delegate sideBarMenu:self didHideAnimated:animated];
                }
            }
        }];
    } else {
        [self moveMenuViewAway];
        [self.menuView setHidden:YES];
        if ([self.delegate respondsToSelector:@selector(sideBarMenu:didHideAnimated:)]) {
            [self.delegate sideBarMenu:self didHideAnimated:animated];
        }
    }
}

- (void)moveMenuViewBack
{
    CGRect newFrame = self.menuView.frame;
    newFrame.origin.x = 0.0;
    [self.menuView setFrame:newFrame];
}

- (void)moveMenuViewAway
{
    CGRect newFrame = self.menuView.frame;
    newFrame.origin.x = newFrame.size.width;
    [self.menuView setFrame:newFrame];
}

- (void)bounceSideBarMenu
{
    // we only allow bounce if the menu is currently hidden
    if (!self.isVisible) {
        // ensure that the menu is in the correct position to start by hiding without animation
        [self hideAnimated:NO];
        // make the menu visible (off screen) before bounce starts
        [self.menuView setHidden:NO];
        
        CGFloat maxBounce = self.menuView.frame.size.width * 0.20f;
        CAKeyframeAnimation *animation = [[self class] bounceAnimationWithMaxBounce:maxBounce];

        [self.menuView.layer addAnimation:animation forKey:@"bouncing"];
        [self performSelector:@selector(bounceAnimCompleted) withObject:nil afterDelay:animation.duration];
    }
}

- (void)bounceAnimCompleted
{
    [self hideAnimated:NO];
}

+ (CAKeyframeAnimation*)bounceAnimationWithMaxBounce:(CGFloat)maxBounce
{
    NSUInteger const kAnimFactors = 22;
    CGFloat const kFactorsPerSec = 36.0f;
    CGFloat factors[kAnimFactors] = {0, 47, 65, 78, 89, 97, 100, 100, 97, 89, 78, 65, 47, 25, 5, 5, 14, 22, 25, 22, 14, 0};

    NSMutableArray* transforms = [NSMutableArray array];

    for (NSUInteger i = 0; i < kAnimFactors; i++)
    {
        CGFloat positionOffset  = -factors[i] / 100 * maxBounce;
        CATransform3D transform = CATransform3DMakeTranslation(positionOffset, 0.0f, 0.0f);

        [transforms addObject:[NSValue valueWithCATransform3D:transform]];
    }

    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.repeatCount           = 1;
    animation.duration              = kAnimFactors * 1.0f/kFactorsPerSec;
    animation.fillMode              = kCAFillModeForwards;
    animation.values                = transforms;
    animation.removedOnCompletion   = YES; // final stage is equal to starting stage
    animation.autoreverses          = NO;

    return animation;
}

- (void)updateEditModeState:(BOOL)editMode
{
    [self.btnEditToggle setSelected:editMode];
}

#pragma mark - State Restoration

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super encodeRestorableStateWithCoder:coder];
    [coder encodeObject:@(self.isVisible) forKey:@"isVisible"];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super decodeRestorableStateWithCoder:coder];
    if ([[coder decodeObjectForKey:@"isVisible"] boolValue]) {
        [self showAnimated:NO];
    } else {
        [self hideAnimated:NO];
    }
}

@end
