//
//  EditorVC.h
//  PhotAnno
//
//  Created by Pete on 26/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnnotationView.h"

@interface EditorVC : UIViewController

@property (strong, nonatomic) UIImage *originalImage;
@property (strong, nonatomic) NSURL *originalImageUrl;
@property (strong, nonatomic) NSDictionary *originalImageMetaData;
@property (weak, nonatomic) IBOutlet AnnotationView *annoView;
@property (weak, nonatomic) IBOutlet UIView *sideBarMenuContainer;
@property (weak, nonatomic) IBOutlet UIView *messageContainer;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
