//
//  MessageVC.m
//  PhotAnno
//
//  Created by Peter on 13/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "MessageVC.h"

@interface MessageVC ()

@end

@implementation MessageVC

#pragma mark - Public Properties

@synthesize message = _message;

- (void)setMessage:(NSString *)message
{
    _message = message;
    if (self.messageLabel) {
        self.messageLabel.text = _message;
    }
}

#pragma mark - Public IBOutlets

@synthesize messageLabel = _messageLabel;

- (void)setMessageLabel:(UILabel *)messageLabel
{
//    [_messageLabel removeObserver:self forKeyPath:@"text"];
    _messageLabel = messageLabel;
    [_messageLabel setText:self.message];
//    [_messageLabel addObserver:self forKeyPath:@"text" options:(NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld) context:NULL];
}

#pragma mark - Lifecycle methods

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.view setClipsToBounds:YES];
    [self.view setBackgroundColor:[UIColor clearColor]];
    [self.view setOpaque:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public methods

- (void)showMessage:(NSString *)message forDuration:(NSTimeInterval)duration
{
    if (message) self.message = message;
    [self showAnimated:YES];
    if (duration > 0) {
        [self performSelector:@selector(hide) withObject:nil afterDelay:duration];
    }
}

- (void)showMessage:(NSString *)message
{
    [self showMessage:message forDuration:0];
}

- (void)showForDuration:(NSTimeInterval)duration
{
    [self showMessage:nil forDuration:duration];
}

- (void)showAnimated:(BOOL)animated
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hide) object:nil];
    if (animated && [self.messageView  isHidden]) {
        // when message view is initialised it may not be in the correct location
        // so we need to ensure that if the messageView is hidden that the animation will start from the away position
        [self moveMessageViewAway];
    }
    [self.messageView setHidden:NO];
    if (animated) {
        [UIView animateWithDuration:0.5 animations:^{
            [self moveMessageViewBack];
        } completion:^(BOOL finished) {
        }];
    } else {
        [self moveMessageViewBack];
    }
}

- (void)hideAnimated:(BOOL)animated
{
    if (animated) {
        [UIView animateWithDuration:0.5 animations:^{
            [self moveMessageViewAway];
        } completion:^(BOOL finished) {
            // hide message even if animation did not finish as it may happen when view is not visible!
//            if (finished) {
                [self.messageView setHidden:YES];
//            }
        }];
    } else {
        [self moveMessageViewAway];
        [self.messageView setHidden:YES];
    }
}

- (void)show
{
    [self showMessage:nil forDuration:0];
}

- (void)hide
{
    [self hideAnimated:YES];
}


#pragma mark - Private methods

- (void)moveMessageViewBack
{
    CGRect newFrame = self.messageView.frame;
    newFrame.origin.y = 0.0;
    [self.messageView setFrame:newFrame];
}

- (void)moveMessageViewAway
{
    CGRect newFrame = self.messageView.frame;
    newFrame.origin.y = newFrame.size.height;
    [self.messageView setFrame:newFrame];
}

//#pragma mark - Key Value Observing
//
//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
//{
//    if (object == self.messageLabel && [keyPath isEqualToString:@"text"])
//    {
//        // check if message property should be updated with label text
//        if (![_message isEqualToString:self.messageLabel.text]) {
//            // use ivar to avoid infinite loop with message setter method
//            _message = self.messageLabel.text;
//        }
//    }
//}

#pragma mark - State Restoration

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super encodeRestorableStateWithCoder:coder];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super decodeRestorableStateWithCoder:coder];
}

@end

