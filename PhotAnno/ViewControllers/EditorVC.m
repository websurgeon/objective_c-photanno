//
//  EditorVC.m
//  PhotAnno
//
//  Created by Pete on 26/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "EditorVC.h"
#import "AnnotationLabel.h"
#import "SideBarMenuVC.h"
#import "MessageVC.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <QuartzCore/QuartzCore.h>

@interface EditorVC () <AnnotationViewDelegate, SideBarMenuDelegate, UIAlertViewDelegate>

@property (nonatomic) CGRect keyboardFrame;
@property (nonatomic) float keyboardYShift;
@property (strong, nonatomic) SideBarMenuVC *sideBarMenuVC;
@property (strong, nonatomic) MessageVC *messageVC;
@property (strong, nonatomic) UISwipeGestureRecognizer *sideBarSwipeGRLeft;
@property (strong, nonatomic) UISwipeGestureRecognizer *sideBarSwipeGRRight;

@end

@implementation EditorVC {
    int _busyCount;
    BOOL _keyboardVisible;
    BOOL _restoringState; // currently using this to control if keyboard animation is to be prevented
}

#pragma mark - Public Properties

@synthesize originalImage = _originalImage;

- (void)setOriginalImage:(UIImage *)originalImage
{
    _originalImage = originalImage;
    [self.annoView setImage:_originalImage];
}

@synthesize originalImageUrl = _originalImageUrl;
@synthesize originalImageMetaData = _originalImageMetaData;

#pragma mark - Public Properties (IBOutlet)

@synthesize annoView = _annoView;

- (void)setAnnoView:(AnnotationView *)annoView
{
    _annoView = annoView;
    [_annoView setImage:self.originalImage];
    [_annoView setDelegate:self];
}

@synthesize sideBarMenuContainer = _sideBarMenuContainer;
@synthesize messageContainer = _messageContainer;
@synthesize activityIndicator = _activityIndicator;

#pragma mark - Private Properties

@synthesize keyboardFrame = _keyboardFrame;
@synthesize keyboardYShift = _keyboardYShift;
@synthesize sideBarMenuVC = _sideBarMenuVC;
@synthesize messageVC = _messageVC;
@synthesize sideBarSwipeGRLeft = _sideBarSwipeGRLeft;

- (UISwipeGestureRecognizer *)sideBarSwipeGRLeft
{
    if (!_sideBarSwipeGRLeft) {
        _sideBarSwipeGRLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGRHandler:)];
        [_sideBarSwipeGRLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    }
    return _sideBarSwipeGRLeft;
}

@synthesize sideBarSwipeGRRight = _sideBarSwipeGRRight;

- (UISwipeGestureRecognizer *)sideBarSwipeGRRight
{
    if (!_sideBarSwipeGRRight) {
        _sideBarSwipeGRRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGRHandler:)];
        [_sideBarSwipeGRRight setDirection:UISwipeGestureRecognizerDirectionRight];
    }
    return _sideBarSwipeGRRight;
}

#pragma mark - Super class method overrides

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.sideBarMenuVC hideAnimated:NO];
    [self.messageVC hideAnimated:NO];
    [self.annoView hideFontResizeSliderAnimated:NO];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"image: %@", self.originalImage);
    NSLog(@"url: %@", [self.originalImageUrl path]);
    NSLog(@"metadata: %@", self.originalImageMetaData);
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:self.view.window];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:self.view.window];
    
    [self.annoView addGestureRecognizer:self.sideBarSwipeGRLeft];
    [self.annoView addGestureRecognizer:self.sideBarSwipeGRRight];

    // if image has not been set then load image from url
    if (!self.originalImage) {
        [self loadImageFromURL:self.originalImageUrl completionBlock:^(UIImage *image, NSDictionary *metaData, NSError *error) {
            self.originalImage = image;
            self.originalImageMetaData = metaData;
            [self.annoView setImage:self.originalImage];
        }];
    } else {
        [self.annoView setImage:self.originalImage];
    }
    if (_keyboardVisible) {
        [self.annoView.currentAnnoItem becomeFirstResponder];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _restoringState = NO; // should have finished all state restoration by now
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:self.view.window];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:self.view.window];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:self.view.window];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:self.view.window];
    [self.annoView removeGestureRecognizer:self.sideBarSwipeGRLeft];
    [self.annoView removeGestureRecognizer:self.sideBarSwipeGRRight];    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Segue methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"sideBarMenuContainerSegue"]) {
        self.sideBarMenuVC = (SideBarMenuVC *)segue.destinationViewController;
        [self.sideBarMenuVC hideAnimated:NO];
        [self.sideBarMenuVC setDelegate:self];
    } else if ([[segue identifier] isEqualToString:@"messageContainerSegue"]) {
        self.messageVC = (MessageVC *)segue.destinationViewController;
    }
}

#pragma mark - keyboard methods

- (void)keyboardWillShow:(NSNotification *)notification
{
    if (_restoringState) [UIView setAnimationsEnabled:NO];
    self.keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    [self moveViewToShowRect:[self.annoView rectForAnnotationItem:self.annoView.currentAnnoItem]];
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    [UIView setAnimationsEnabled:YES];
    _keyboardVisible = YES;
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    self.keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    [self moveViewBackToNormal];
}

- (void)keyboardDidHide:(NSNotification *)notification
{
    _keyboardVisible = NO;
}

- (void)moveViewToShowRect:(CGRect)rect
{
    CGRect tapInView = [self.annoView convertRect:rect fromView:self.annoView];
    CGPoint keyboardInView = [self.annoView convertPoint:self.keyboardFrame.origin fromView:self.view.window];
    // adjust keyboard location for current shift
    keyboardInView.y += self.keyboardYShift;

    CGRect frame = self.annoView.frame;
    // remove current shift so that keyboardYShift can be recalculated fresh
    frame.origin.y -= self.keyboardYShift;
    self.keyboardYShift = 0;

    float padding = (keyboardInView.y - rect.size.height) / 2.0; // shift view to make item in center of remaining view above keyboard
    float showY = (tapInView.origin.y + tapInView.size.height + padding);
    if (showY > keyboardInView.y) {
        self.keyboardYShift = keyboardInView.y - showY;
        frame.origin.y += self.keyboardYShift;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        [self.annoView setFrame:frame];
        [UIView commitAnimations];
    }
}

- (void)moveViewBackToNormal
{
    CGRect frame = self.annoView.frame;
    frame.origin.y -= self.keyboardYShift;
    self.keyboardYShift = 0;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    [self.annoView setFrame:frame];
    [UIView commitAnimations];
}

#pragma mark - GestureRecognizer handler methods

// UIGestureRecognizerDelegate method
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gr shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGr
{
    
    //    if(gr == self.dragAnnoGR && (otherGr == self.sideBarSwipeGRLeft || otherGr == self.sideBarSwipeGRLeft)) {
    //        // allow swipe gestures to show/hide sideBar while also allowing annotation dragging
    //        return YES;
    //    }
    
    // may as well just return YES until there is a case were we need to prevent simultaneous gestures
    return YES;
}


- (void)swipeGRHandler:(UISwipeGestureRecognizer *)gr
{
    if ([gr state] == UIGestureRecognizerStateRecognized) {
        if (![self.annoView isDraggingAnnotation] && ([gr isEqual:self.sideBarSwipeGRLeft] || [gr isEqual:self.sideBarSwipeGRRight])) {
            if ([gr direction] == UISwipeGestureRecognizerDirectionLeft) {
                [self.sideBarMenuVC showAnimated:YES];
                [self.view endEditing:YES];
                [self moveViewBackToNormal];
            } else {
                [self.sideBarMenuVC hideAnimated:YES];
            }
        }
    }
}

#pragma mark - Private methods

- (void)startBusyIndicator
{
    if (_busyCount == 0) {
        [self.activityIndicator startAnimating];
    }
    _busyCount += 1;
}

- (void)endBusyIndicatorAndForceStop:(BOOL)forceStop
{
    _busyCount -= 1;
    if (forceStop || _busyCount < 0) _busyCount = 0;
    if (_busyCount == 0) {
        [self.activityIndicator stopAnimating];
    }
}

- (void)endBusyIndicator
{
    [self endBusyIndicatorAndForceStop:NO];
}

- (void)saveImage:(UIImage *)image withMetaData:(NSDictionary *)metaData completionBlock:(void (^)(NSURL *assetURL, NSError *error))completionBlock
{
    dispatch_queue_t queue = dispatch_queue_create("save photo", NULL);
    dispatch_async(queue, ^{
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        NSMutableDictionary *newMetaData = [metaData mutableCopy];
        [newMetaData setObject:@((ALAssetOrientation)image.imageOrientation) forKey:@"Orientation"];
        [library writeImageToSavedPhotosAlbum:[image CGImage] metadata:newMetaData completionBlock:^(NSURL *assetURL, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionBlock(assetURL, error);
            });
        }];
    });
}

- (void)loadImageFromURL:(NSURL *)url completionBlock:(void (^)(UIImage *image, NSDictionary *metaData, NSError *error))completionBlock
{
    NSError *error = nil;
    if (url) {
        ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
        [assetslibrary assetForURL:url
                       resultBlock:^(ALAsset *asset) {
                           ALAssetRepresentation *rep = [asset defaultRepresentation];
                           CGImageRef iref = [rep fullResolutionImage];
                           NSDictionary *metaData = rep.metadata;
                           float scale = [rep scale];
                           ALAssetOrientation orientation = [rep orientation];
                           if (iref) {
                               completionBlock([UIImage imageWithCGImage:iref scale:scale orientation:(UIImageOrientation)orientation], metaData, error);
                           } else {
                               completionBlock(nil, metaData, error);
                           }
                       }
                      failureBlock:^(NSError *error) {
                          NSLog(@"Can't load image - %@",[error localizedDescription]);
                          completionBlock(nil, nil, error);
                      }];
    }
}

- (void)close
{
    [self performSegueWithIdentifier:@"unwindToImageSource" sender:self];
}

#pragma mark - AnnotationViewDelegate methods

- (void)didTapAnnotationView:(AnnotationView *)annotationView atPoint:(CGPoint)point
{
    if ([self.sideBarMenuVC isVisible]) {
        [self.sideBarMenuVC hideAnimated:YES];
    } else {
        if (!annotationView.editMode) {
            if (self.annoView.currentAnnoItem) {
                [self.annoView.currentAnnoItem resignFirstResponder];
                self.annoView.currentAnnoItem = nil;
            } else {
                if ([self.annoView annotationImageContainsPoint:point]) {
                    AnnotationLabel *lbl = [[AnnotationLabel alloc] init];
                    [self.annoView addAnnotationItem:lbl atPoint:point];
                    self.annoView.currentAnnoItem = lbl;
                    [lbl becomeFirstResponder];
                } else {
//                    NSLog(@"TAP OUTSIDE OF IMAGE: %@", NSStringFromCGPoint(point));
                }
            }            
        } else {
            AnnotationItemView *item = self.annoView.currentAnnoItem;
            if ([item isFirstResponder]) {
                [self moveViewToShowRect:[self.annoView rectForAnnotationItem:item]];
            }
        }
    }
}

- (void)annotationView:(AnnotationView *)annotationView editModeChanged:(BOOL)editMode
{
    [self.sideBarMenuVC updateEditModeState:self.annoView.editMode];
    [self.messageVC showMessage:[NSString stringWithFormat:@"Edit Mode %@",self.annoView.editMode ? @"ON" : @"OFF"] forDuration:3.0];
}

#pragma mark - SideBarMenuVC delegate methods

- (void)didTapCloseOnSideBarMenu:(SideBarMenuVC *)sideBarMenu
{
    if (self.annoView.hasUnsavedChanges) {
        UIAlertView *confirmClose = [[UIAlertView alloc] initWithTitle:@"Close without saving?" message:@"There are unsaved changes. Are you sure you want to close?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        [confirmClose show];
    } else {
        [self close];
    }
}

- (void)didTapSaveOnSideBarMenu:(SideBarMenuVC *)sideBarMenu
{
    [self.sideBarMenuVC hideAnimated:YES];
    [self startBusyIndicator];
    [self.annoView createAnnotationImageWithCompletionBlock:^(UIImage *image) {
        [self saveImage:image withMetaData:self.originalImageMetaData completionBlock:^(NSURL *assetURL, NSError *error) {
            [self endBusyIndicator];
            if (error == nil) {
                self.annoView.hasUnsavedChanges = NO;
                NSLog(@"SAVED: %@", [assetURL path]);
                [self.messageVC showMessage:@"Saved to Photo Library!" forDuration:3.0];
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SAVE FAILED!" message:@"Please try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [self.messageVC showMessage:@"SAVE FAILED!" forDuration:3.0];
                [alert show];
            }
        }];
    }];
}

- (BOOL)didTapToggleEditModeOnSideBarMenu:(SideBarMenuVC *)sideBarMenu
{
    [self.annoView setEditMode:!self.annoView.editMode];
    return self.annoView.editMode;
}

- (void)sideBarMenu:(SideBarMenuVC *)sideBarMenu didShowAnimated:(BOOL)animated
{
    // allow sideBarContainer to handling gestures while it is visible
    [self.sideBarMenuContainer setUserInteractionEnabled:YES];
}

- (void)sideBarMenu:(SideBarMenuVC *)sideBarMenu didHideAnimated:(BOOL)animated
{
    // prevent sideBarContainer from handling gestures while it is hidden
    [self.sideBarMenuContainer setUserInteractionEnabled:NO];
}

#pragma mark - UIAlertViewDelegate methods

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self close];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle 
{ 
    return UIStatusBarStyleLightContent; 
}

#pragma mark - State Restoration

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super encodeRestorableStateWithCoder:coder];
   
    // decided to encode the image and metadata instead of later loading from url as this seems to be taking too long!
    [coder encodeObject:self.originalImageUrl forKey:@"originalImageUrl"];
    [coder encodeObject:self.originalImage forKey:@"originalImage"];
    [coder encodeObject:self.originalImageMetaData forKey:@"originalImageMetaData"];
    [coder encodeObject:@(_keyboardVisible) forKey:@"keyboardVisible"];
    
    // store the sideBarVC so that it can be part of the restore chain
    [coder encodeObject:self.sideBarMenuVC forKey:@"sideBarVC"];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super decodeRestorableStateWithCoder:coder];

    _restoringState = YES;
    
    self.originalImageUrl = [coder decodeObjectForKey:@"originalImageUrl"];
    self.originalImage = [coder decodeObjectForKey:@"originalImage"];
    self.originalImageMetaData = [coder decodeObjectForKey:@"originalImageMetaData"];
    _keyboardVisible = [[coder decodeObjectForKey:@"keyboardVisible"] boolValue];
    
//    self.sideBarMenuVC = [coder decodeObjectForKey:@"sideBarVC"]; // Not required as the vc will restore itself!
}

@end
