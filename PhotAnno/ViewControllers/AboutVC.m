//
//  AboutVC.m
//  PhotAnno
//
//  Created by Pete on 04/11/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "AboutVC.h"

@interface AboutVC () <UITextViewDelegate>

@end

@implementation AboutVC

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    [self setup];
    
    if ([self respondsToSelector:@selector(topLayoutGuide)]) {
        id label = self.titleLabel;
        [label setTranslatesAutoresizingMaskIntoConstraints: NO];
        id topGuide = self.topLayoutGuide;
        NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings (label, topGuide);
        
        [self.view addConstraints:
         [NSLayoutConstraint constraintsWithVisualFormat: @"V:[topGuide]-0-[label]"
                                                 options: 0
                                                 metrics: nil
                                                   views: viewsDictionary]
         ];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setup
{
    [self loadAppDescriptionText];
}

- (void)loadAppDescriptionText
{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"about" ofType:@"txt"];
    NSString *text = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];

    [self.textView setEditable:NO];
    [self.textView setDataDetectorTypes:UIDataDetectorTypeLink];    

    // link detection in iOS7 requires that the textView be selectable
    if ([self.textView respondsToSelector:@selector(setSelectable:)]) {
        [self.textView setSelectable:YES];

        // A bug in iOS7 is causes the font to be reset when setText is called
        // http://stackoverflow.com/questions/19049917/uitextview-font-is-being-reset-after-settext
        // setting 'selectable' seems to prevent this issue!
        [self.textView setText:text];
    } else {
        [self.textView setText:text];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle 
{ 
    return UIStatusBarStyleLightContent; 
}

@end
