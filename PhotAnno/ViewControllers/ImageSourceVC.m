//
//  ImageSourceVC.m
//  PhotAnno
//
//  Created by Pete on 26/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "ImageSourceVC.h"
#import "EditorVC.h"
#import <AssetsLibrary/AssetsLibrary.h>

#define SEGUE_TO_EDITOR @"segue_imageSource_to_editor"

@interface ImageSourceVC ()

@property (strong, nonatomic) UIPopoverController *imagePickerPopoverController;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) NSURL *imageUrl;
@property (strong, nonatomic) NSDictionary *imageMetaData;

@end

@implementation ImageSourceVC

#pragma mark - Public Properties (IBOutlets)

@synthesize backgroundImageView = _backgroundImageView;
@synthesize btn_pickImage = _btn_pickImage;
@synthesize btn_takePhoto = _btn_takePhoto;
@synthesize activityIndicator = _activityIndicator;

#pragma mark - Public Properties

@synthesize imagePickerController = _imagePickerController;

- (UIImagePickerController *)imagePickerController
{
    if (!_imagePickerController) {
        _imagePickerController = [[UIImagePickerController alloc] init];        
        _imagePickerController.delegate = self;
    }
    return _imagePickerController;
}

#pragma mark - Private Properties

@synthesize imagePickerPopoverController = _imagePickerPopoverController;

- (UIPopoverController *)imagePickerPopoverController
{
    if (!_imagePickerPopoverController) {
        _imagePickerPopoverController = [[UIPopoverController alloc] initWithContentViewController:self.imagePickerController];
        _imagePickerPopoverController.delegate = self;
    }
    return _imagePickerPopoverController;
}

#pragma mark - Super class method overrides

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // ensure that image picker is initialise before button is tapped to avoid slight delay!
    [self imagePickerController];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self imagePickerPopoverController];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Segue methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:SEGUE_TO_EDITOR]) {
        EditorVC *vc  = segue.destinationViewController;
//        [vc setOriginalImage:self.image];
        [vc setOriginalImageUrl:self.imageUrl];
//        [vc setOriginalImageMetaData:self.imageMetaData];
    }
}

#pragma mark - public methods (IBActions)

- (void)showImagePicker:(id)sender
{
    [self showImagePicker];
}

- (void)launchCamera:(id)sender
{
    [self launchCamera];
}

- (void)unwindToImageSource:(UIStoryboardSegue *)segue
{
    
}

#pragma mark - UIImagePickerController delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if ([picker isEqual:self.imagePickerController]) {
        [self hideImagePicker];
        [self.activityIndicator startAnimating];
        if ([picker sourceType] == UIImagePickerControllerSourceTypeCamera) {
            [ImageSourceVC saveImageFromInfo:info completionBlock:^(NSURL *assetURL, NSError *error) {
                [self imagePicked:[info objectForKey:UIImagePickerControllerOriginalImage] withURL:assetURL andMetaData:[info objectForKey:UIImagePickerControllerMediaMetadata]];
                [self.activityIndicator stopAnimating];
            }];
        } else {
            NSURL *url = [info objectForKey:UIImagePickerControllerReferenceURL];
            // metadata is only availalble when image is from camera source and so we need to load the image!
            [ImageSourceVC getImageForUrl:url completionBlock:^(UIImage *image, NSDictionary *metaData) {
                [self imagePicked:image withURL:url andMetaData:metaData];
                [self.activityIndicator stopAnimating];
            }];
        }
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    if ([picker isEqual:self.imagePickerController]) {
        [self imagePickerCancelled];
    }
}

#pragma mark - UIPopoverController delegate methods

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    if ([popoverController isEqual:self.imagePickerPopoverController]) {
        // no action required
    }
}

#pragma mark - private methods

- (void)imagePicked:(UIImage *)image withURL:(NSURL *)url andMetaData:(NSDictionary *)metaData
{
    self.image = image;
    self.imageMetaData = metaData;
    self.imageUrl = url;
    [self performSegueWithIdentifier:SEGUE_TO_EDITOR sender:self.imagePickerController];
}

- (void)showImagePicker
{
    UIImagePickerController *picker = self.imagePickerController;
    [picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [self presentViewController:picker animated:YES completion:^{
            // no action required
        }];
    } else {
        CGSize viewSize = self.view.frame.size;
        CGRect rect = CGRectMake(viewSize.width / 2, viewSize.height / 2, 1, 1);
        [self.imagePickerPopoverController presentPopoverFromRect:rect inView:self.view permittedArrowDirections:0 animated:YES];
        
        // TODO: find out what is causing warning at run time...
        // WARNING: no index path for table cell being reused
    }
}

- (void)launchCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [self.imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
        [self presentViewController:self.imagePickerController animated:YES completion:^{
            // no action required
        }];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Device does not support camera usage!" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (void)hideImagePicker
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            [self dismissViewControllerAnimated:YES completion:^{
                // no action required
            }];
    } else {
        [self.imagePickerPopoverController dismissPopoverAnimated:YES];
    }
}

- (void)imagePickerCancelled
{
    [self hideImagePicker];
}

#pragma mark - Public Class methods

+ (void)getImageForUrl:(NSURL *)url completionBlock:(void (^)(UIImage *image, NSDictionary *metaData))completionBlock {
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    
    [assetslibrary assetForURL:url resultBlock:^(ALAsset *asset) {
        
        ALAssetRepresentation *rep = [asset defaultRepresentation];
        CGImageRef iref = [rep fullResolutionImage];
        float scale = [rep scale];
        ALAssetOrientation orientation = [rep orientation];
        if (iref) {
            completionBlock([UIImage imageWithCGImage:iref scale:scale orientation:(UIImageOrientation)orientation], rep.metadata);
        } else {
            completionBlock(nil, rep.metadata);
        }
    } failureBlock:^(NSError *error) {
        NSLog(@"Can't load image - %@",[error localizedDescription]);
        completionBlock(nil, nil);
    }];
}

#pragma mark - Private Class methods

+ (void)saveImageFromInfo:(NSDictionary *)info completionBlock:(void (^)(NSURL *assetURL, NSError *error))saveComplete
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    dispatch_queue_t queue = dispatch_queue_create("save photo", NULL);
    dispatch_async(queue, ^{
        [library writeImageToSavedPhotosAlbum:[image CGImage] metadata:[info objectForKey:UIImagePickerControllerMediaMetadata] completionBlock:^(NSURL *assetURL, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                saveComplete(assetURL, error);
            });
        }];
    });
}

-(UIStatusBarStyle)preferredStatusBarStyle 
{ 
    return UIStatusBarStyleLightContent; 
}

#pragma mark - State Restoration

// NB: there is currently no state that needs restoring for this view controller!

//- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
//{
//    [super encodeRestorableStateWithCoder:coder];
//}
//
//- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
//{
//    [super decodeRestorableStateWithCoder:coder];
//}

@end
