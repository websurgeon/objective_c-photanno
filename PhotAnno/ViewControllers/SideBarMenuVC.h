//
//  SideBarMenuVC.h
//  PhotAnno
//
//  Created by Pete on 19/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SideBarMenuVC;

@protocol SideBarMenuDelegate <NSObject>

@optional

- (void)sideBarMenu:(SideBarMenuVC *)sideBarMenu didTapMenuItem:(id)sender;
- (void)didTapCloseOnSideBarMenu:(SideBarMenuVC *)sideBarMenu;
- (void)didTapSaveOnSideBarMenu:(SideBarMenuVC *)sideBarMenu;
- (BOOL)didTapToggleEditModeOnSideBarMenu:(SideBarMenuVC *)sideBarMenu; // returns resulting edit mode state

- (void)sideBarMenu:(SideBarMenuVC *)sideBarMenu willShowWithDuration:(NSTimeInterval)duration;
- (void)sideBarMenu:(SideBarMenuVC *)sideBarMenu didShowAnimated:(BOOL)animated;

- (void)sideBarMenu:(SideBarMenuVC *)sideBarMenu willHideWithDuration:(NSTimeInterval)duration;
- (void)sideBarMenu:(SideBarMenuVC *)sideBarMenu didHideAnimated:(BOOL)animated;

@end

@interface SideBarMenuVC : UIViewController

@property (assign, nonatomic) IBOutlet id <SideBarMenuDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *menuView; // the view that is shown/hidden. self.view is just a transparent container
@property (weak, nonatomic) IBOutlet UIButton *btnEditToggle;

@property (nonatomic, readonly) BOOL isVisible;

- (void)showAnimated:(BOOL)animated;
- (void)hideAnimated:(BOOL)animated;
- (void)bounceSideBarMenu;

- (IBAction)closePhoto:(id)sender;
- (IBAction)save:(id)sender;
- (IBAction)toggleEditMode:(id)sender;
- (void)updateEditModeState:(BOOL)editMode;

@end
