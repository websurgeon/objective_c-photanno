//
//  ImageSourceVC.h
//  PhotAnno
//
//  Created by Pete on 26/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageSourceVC : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPopoverControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIButton *btn_pickImage;
@property (weak, nonatomic) IBOutlet UIButton *btn_takePhoto;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) UIImagePickerController *imagePickerController;

- (IBAction)showImagePicker:(id)sender;

- (IBAction)launchCamera:(id)sender;

- (IBAction)unwindToImageSource:(UIStoryboardSegue *)segue;

- (void)imagePicked:(UIImage *)image withURL:(NSURL *)url andMetaData:(NSDictionary *)metaData;

+ (void)getImageForUrl:(NSURL *)url completionBlock:(void (^)(UIImage *image, NSDictionary *metaData))completionBlock;

@end
