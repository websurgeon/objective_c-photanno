//
//  AnnotationItemView.m
//  PhotAnno
//
//  Created by Pete on 28/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "AnnotationItemView.h"

@interface AnnotationItemView ()

@property (strong, nonatomic) UIButton *delButton;
@property (strong, nonatomic) UIView *borderView;
@property (strong, nonatomic) NSString *guid;

@end

@implementation AnnotationItemView

#pragma mark - Private Properties

@synthesize delButton = _delButton;

- (UIButton *)delButton
{
    if (!_delButton) {
        _delButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_delButton setBackgroundColor:[UIColor redColor]];
        [[_delButton titleLabel] setTextColor:[UIColor whiteColor]];
        float fontSize = 17.0f;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            fontSize = 34.0f;
        }
        [_delButton.titleLabel setFont:[_delButton.titleLabel.font fontWithSize:fontSize]];
        [_delButton setTitle:@"X" forState:UIControlStateNormal];
        [_delButton setHidden:!self.editMode];
        [_delButton sizeToFit];
        CGSize size = _delButton.frame.size;
        [_delButton setFrame:CGRectMake(0, -size.height, size.width, size.height)];
        [_delButton addTarget:self action:@selector(didTapDelete:) forControlEvents:UIControlEventTouchUpInside];
        [_delButton setTag:123];
        [self addSubview:_delButton];
    }
    return _delButton;
}

- (void)setDelButton:(UIButton *)delButton
{
    _delButton = delButton;
    [_delButton setHidden:!self.editMode];
    [_delButton addTarget:self action:@selector(didTapDelete:) forControlEvents:UIControlEventTouchUpInside];
}

@synthesize borderView = _borderView;

- (UIView *)borderView
{
    if (!_borderView) {
        _borderView = [[UIView alloc] init];
        [[_borderView layer] setBorderWidth:1.0];
        [[_borderView layer] setBorderColor:[[UIColor redColor] CGColor]];   //Adding Border color.
        [self addSubview:_borderView];
        [_borderView setOpaque:NO];
        [_borderView setHidden:!self.isSelected];
        [self bringSubviewToFront:_borderView];
    }
    return _borderView;
}

@synthesize guid = _guid;

- (NSString *)guid
{
    if (!_guid) {
        _guid = GenerateGUID();
    }
    return _guid;
}

#pragma mark - Public Properties

@synthesize delegate = _delegate;
@synthesize editMode = _editMode;

- (void)setEditMode:(BOOL)editMode
{
    BOOL oldMode = _editMode;
    _editMode = editMode;
    if (_editMode != oldMode) {
        // store selected state so that we can reset the value after edit mode
        [self updateViewForEditMode:_editMode];
    }
}

@synthesize isSelected = _isSelected;

- (void)setIsSelected:(BOOL)isSelected
{
    _isSelected = isSelected;
    [self updateViewForEditMode:self.editMode];
}

- (void)updateViewForEditMode:(BOOL)editMode {
    if (self.editMode) {
        [self showDeleteButton];
        [self.borderView setHidden:!self.isSelected];
    } else {
        [self hideDeleteButton];
        [self.borderView setHidden:YES];
    }
}

#pragma mark - Super Class Overrides

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSetup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initSetup];
        self.delButton = (UIButton *)[self viewWithTag:123];
        [self setEditMode:[[aDecoder decodeObjectForKey:@"editMode"] boolValue]];
        [self setIsSelected:[[aDecoder decodeObjectForKey:@"isSelected"] boolValue]];
        [self setGuid:[aDecoder decodeObjectForKey:@"guid"]];
    }
    return self;
}

- (void)initSetup
{
    // ensure that position and size of annotation item resizes correctly with superview resising
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    [self setUserInteractionEnabled:NO];

}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:@(self.editMode) forKey:@"editMode"];
    [aCoder encodeObject:@(self.isSelected) forKey:@"isSelected"];
    [aCoder encodeObject:self.guid forKey:@"guid"];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [self redrawBorder];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self redrawBorder];
}

- (void)redrawBorder
{
//    if (self.borderView.hidden) return;
    float padding = 4.0f;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        padding = 8.0f;
    }
    CGRect frame = self.bounds;
    frame.origin.x = -padding;
    frame.origin.y = -padding;
    frame.size.width += (2 * padding);
    frame.size.height += (2 * padding);
    [self.borderView setFrame:frame];
}
#pragma mark - Private methods

- (void)showDeleteButton
{
    [self.delButton setHidden:NO];
}

- (void)hideDeleteButton
{
    [self.delButton setHidden:YES];
}

#pragma mark - Button Action

- (void)didTapDelete:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didTapDeleteButtonForAnnotationItemView:)]) {
        [self.delegate didTapDeleteButtonForAnnotationItemView:self];
    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    if (self.editMode && CGRectContainsPoint(self.delButton.frame, point)) {
        return self.delButton;
    }
    return [super hitTest:point withEvent:event];
}

- (CGRect)frameIncludingDelButton
{
    CGRect frame = self.frame;
    frame.origin.y = frame.origin.y - self.delButton.bounds.size.height;
    frame.size.height = frame.size.height + self.delButton.bounds.size.height;
    return frame;
}

// not currently a managed object but it is useful to be able to identify an item uniquely
- (NSString *)modelIdentifier
{
    return self.guid;
}

NSString *GenerateGUID() {
    // Create universally unique identifier (object)
    CFUUIDRef uuidObject = CFUUIDCreate(kCFAllocatorDefault);
    
    // Get the string representation of CFUUID object.
    NSString *uuidStr = (NSString *)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, uuidObject));
    
    // If needed, here is how to get a representation in bytes, returned as a structure
    // typedef struct {
    //   UInt8 byte0;
    //   UInt8 byte1;
    //   ...
    //   UInt8 byte15;
    // } CFUUIDBytes;
    //CFUUIDBytes bytes = CFUUIDGetUUIDBytes(uuidObject);
    
    return uuidStr;
}

@end
