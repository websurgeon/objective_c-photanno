//
//  AnnotationItemView.h
//  PhotAnno
//
//  Created by Pete on 28/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AnnotationItemView;

@protocol AnnotationItemViewDelegate <NSObject, NSCoding>

@optional

- (void)didTapDeleteButtonForAnnotationItemView:(AnnotationItemView *)item;

@end

@interface AnnotationItemView : UIView

@property (assign, nonatomic) id <AnnotationItemViewDelegate> delegate;
@property (nonatomic) BOOL editMode;
@property (nonatomic) BOOL isSelected;

- (id)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)aCoder;
- (NSString *)modelIdentifier;
- (CGRect)frameIncludingDelButton;

@end
