//
//  ExpandedHitMarginView.m
//  PhotAnno
//
//  Created by Pete on 31/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "ExpandedHitMarginView.h"

@implementation ExpandedHitBoundaryView

@synthesize top = _top;
@synthesize right = _right;
@synthesize bottom = _bottom;
@synthesize left = _left;

- (id)initWithFrame:(CGRect)frame topMargin:(float)top right:(float)right bottom:(float)bottom left:(float)left
{
    self = [super initWithFrame:frame];
    if (self) {
        self.top = top;
        self.right = right;
        self.bottom = bottom;
        self.left = left;
    }
    return self;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    CGRect frame = self.bounds;
    frame.origin.y -= self.top;
    frame = CGRectMake(self.bounds.origin.x - self.left, 
                       self.bounds.origin.y - self.top,
                       self.bounds.size.width + self.left + self.right,
                       self.bounds.size.height + self.top + self.bottom);
    if (CGRectContainsPoint(frame, point)) {
        return YES;
    }
    return [super pointInside:point withEvent:event];
}

    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     - (void)drawRect:(CGRect)rect
     {
     // Drawing code
     }
     */

@end
