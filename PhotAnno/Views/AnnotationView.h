//
//  AnnotationView.h
//  PhotAnno
//
//  Created by Pete on 26/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnnotationItemView.h"

@class AnnotationView;

@protocol AnnotationViewDelegate <NSObject>

@optional

- (void)didTapAnnotationView:(AnnotationView *)annotationView atPoint:(CGPoint)point;

- (void)annotationView:(AnnotationView *)annotationView editModeChanged:(BOOL)editMode;

@end

@interface AnnotationView : UIView <AnnotationItemViewDelegate>

@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (assign, nonatomic) id <AnnotationViewDelegate> delegate;
@property (strong, nonatomic) UIImage *image;
@property (nonatomic) BOOL editMode;
@property (nonatomic) BOOL isDraggingAnnotation;
@property (nonatomic) BOOL hasUnsavedChanges;
@property (strong, nonatomic) AnnotationItemView *currentAnnoItem;

- (void)addAnnotationItem:(AnnotationItemView *)item atPoint:(CGPoint)point;

- (void)removeAnnotationItem:(AnnotationItemView *)item animated:(BOOL)animated;

- (BOOL)annotationImageContainsPoint:(CGPoint)point;

- (BOOL)annotationImageContainsRect:(CGRect)rect;

- (void)createAnnotationImageWithCompletionBlock:(void (^)(UIImage *image))completionBlock;

- (CGRect)rectForAnnotationItem:(AnnotationItemView *)item;

- (void)showFontResizeSliderAnimated:(BOOL)animated;

- (void)hideFontResizeSliderAnimated:(BOOL)animated;

+ (CGRect)rectForImage:(UIImage *)image whenAspectFitToRect:(CGRect)rect;

- (IBAction)fontResizeSliderValueChanged:(UISlider *)slider;

@end
