//
//  AnnotationView.m
//  PhotAnno
//
//  Created by Pete on 26/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "AnnotationView.h"
#import "AnnotationItemView.h"
#import "AnnotationLabel.h"
#import "ExpandedHitMarginView.h"
#import <QuartzCore/QuartzCore.h>

#define DRAG_BORDER_PADDING 10.0
#define MAX_DELETE_BUTTON_HEIGHT 20.0 // used to expand hit margin on annoView. TODO: need to use this value when creating delete buttons too!?!

@interface AnnotationView () <UIGestureRecognizerDelegate> {

}

@property (strong, nonatomic) UITapGestureRecognizer *tapGR;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) ExpandedHitBoundaryView *annoView;
@property (strong, nonatomic) NSMutableArray *annoItems;
@property (strong, nonatomic) UIPanGestureRecognizer *dragAnnoGR;
@property (strong, nonatomic) UILongPressGestureRecognizer *longPressGR;

@end

@implementation AnnotationView {
    CGPoint _panCoord;
    BOOL _itemLongStarted;
}

#pragma mark - Public Properties

@synthesize  slider = _slider;

@synthesize image = _image;

- (void)setImage:(UIImage *)image
{
    _image = image;
    [self.imageView setImage:_image];
    [self setNeedsLayout];
}

@synthesize editMode = _editMode;

- (void)setEditMode:(BOOL)editMode
{
    BOOL oldEditMode = _editMode;
    _editMode = editMode;
    if (editMode != oldEditMode) {
        [self annotationItemsSetEditMode:editMode];
        [self makeAnnotationsDraggable:editMode];
        if ([self.delegate respondsToSelector:@selector(annotationView:editModeChanged:)]) {
            [self.delegate annotationView:self editModeChanged:editMode];
        }
    }
    if (!editMode) {
        [self deselectAllAnnotationItems];
        [self hideFontResizeSliderAnimated:YES];
    }
}

@synthesize isDraggingAnnotation = _isDraggingAnnotation;

@synthesize hasUnsavedChanges = _hasUnsavedChanges;

#pragma mark - Private Properties

@synthesize annoItems = _annoItems;

- (NSMutableArray *)annoItems
{
    if (!_annoItems) {
        _annoItems = [NSMutableArray array];
    }
    return _annoItems;
}

@synthesize imageView = _imageView;

- (void)setImageView:(UIImageView *)imageView
{
    _imageView = imageView;
    [_imageView setImage:self.image];
}

@synthesize annoView = _annoView;

@synthesize dragAnnoGR = _dragAnnoGR;

- (UIPanGestureRecognizer *)dragAnnoGR
{
    if (!_dragAnnoGR) {
        _dragAnnoGR  = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragging:)];
        [_dragAnnoGR setDelegate:self];
        [_dragAnnoGR setCancelsTouchesInView:NO];
    }
    return _dragAnnoGR;
}

@synthesize longPressGR = _longPressGR;

- (UILongPressGestureRecognizer *)longPressGR
{
    if (!_longPressGR) {
        _longPressGR = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    }
    return _longPressGR;
}

@synthesize currentAnnoItem = _currentAnnoItem;

- (void)setCurrentAnnoItem:(AnnotationItemView *)currentAnnoItem
{
    if (![_currentAnnoItem isEqual:currentAnnoItem]) {
        _currentAnnoItem = currentAnnoItem;
        [self deselectAllAnnotationItems];
        [_currentAnnoItem setIsSelected:YES];
    }
}

#pragma mark - Super class method overrides

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initSetup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initSetup];
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect imageRect = [AnnotationView rectForImage:self.image whenAspectFitToRect:self.frame];
    [self.imageView setFrame:imageRect];
    [self.imageView setNeedsLayout];
    [self.annoView setFrame:imageRect];
    [self.annoView setNeedsLayout];
    
    if (!CGRectIsEmpty(imageRect)) {
        [self layoutAnnoItems];
    }
}

#pragma mark - Public methods

- (void)addAnnotationItem:(AnnotationItemView *)item atPoint:(CGPoint)point
{    
    CGRect frame = item.frame;
    frame.origin = [self.annoView convertPoint:point fromView:self];
    [item setFrame:frame];
    [item setDelegate:self];
    // TODO: would be better to split this functionality into a couple of methods (rather than use if statement)!
    if (![self.annoItems containsObject:item]) [self.annoItems addObject:item];
    [self.annoView addSubview:item];
    self.hasUnsavedChanges = YES;
}

- (void)removeAnnotationItem:(AnnotationItemView *)item animated:(BOOL)animated
{
    // clip to bounds so that del button is hidden
    [item setClipsToBounds:YES];
    // create zero size frame
    CGRect newFrame = item.frame;
    newFrame.size = CGSizeZero;
    // start animation
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         // item shrink
                         [item setFrame:newFrame];
                         // item fade
                         [item setAlpha:0.0];
                     }
                     completion:^(BOOL finished){
                         if (finished) {
                             // actually remove the item
                             [self.annoItems removeObject:item];
                             if ([self.annoItems count] == 0) {
                                 [self setEditMode:NO];
                             }
                         }
                     }];
    self.hasUnsavedChanges = YES;
}

- (BOOL)annotationImageContainsPoint:(CGPoint)point
{
    return CGRectContainsPoint(self.annoView.frame, point);
}

- (BOOL)annotationImageContainsRect:(CGRect)rect
{
    return CGRectContainsRect(self.annoView.frame, rect);
}


- (CGRect)rectForAnnotationItem:(AnnotationItemView *)item
{
    return [self convertRect:item.frame fromView:self.annoView];
}

- (void)createAnnotationImageWithCompletionBlock:(void (^)(UIImage *image))completionBlock
{
    [self annotationItemsSetEditMode:NO];
    dispatch_queue_t queue = dispatch_queue_create("create annotated image", NULL);
    dispatch_async(queue, ^{
        UIImage *originalImage = self.image;
        CGRect annoRect = [AnnotationView rectForImage:originalImage whenAspectFitToRect:self.annoView.frame];
        
//        float scale = (originalImage.size.width / annoRect.size.width); // scaling the annotation image this way was causing a memory pressure crash on device (iPhone 4) so need to avoid scaling until further investigations can be done
        float scale = 1; 
        UIImage *annoImage = [AnnotationView imageFromView:self.annoView useRect:annoRect scaleFactor:scale];
        
        UIGraphicsBeginImageContextWithOptions(originalImage.size, self.annoView.opaque, 0.0f);
        
        CGRect rect = CGRectMake(0, 0, originalImage.size.width, originalImage.size.height);
        
        [originalImage drawInRect:rect];
        
        [annoImage drawInRect:rect];
        
        UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();

        dispatch_async(dispatch_get_main_queue(), ^{
            [self annotationItemsSetEditMode:self.editMode];
            completionBlock(resultingImage);
        });
    });
}

- (void)showFontResizeSliderAnimated:(BOOL)animated
{
    AnnotationLabel *item = (AnnotationLabel *)self.currentAnnoItem;
    UISlider *slider = self.slider;

//-------------------------------
    // This is a quick an dirty workaround to fix a layout issue in iOS 6
    // Slider is getting repositioned to the IB location by autolayout constraints
    // The slider position really needs to be properly controlled by constrains rather than repositioning frame
    [slider removeFromSuperview];
    [slider setTranslatesAutoresizingMaskIntoConstraints:YES];
    [slider setFrame:slider.frame];
    [self addSubview:slider];
//-------------------------------
    
    float min = [item fontMinSize];
    float max = [item fontMaxSize];
    [slider setMinimumValue:min];
    [slider setMaximumValue:max];
    [slider setValue:item.fontSize animated:NO];
    [self bringSubviewToFront:slider];
    [slider setAlpha:0.0];
    [slider setHidden:NO];
    [self positionFontResizeSliderForAnnotationItem:item animated:NO];
    if (animated) {
        [UIView animateWithDuration:0.15 animations:^{
            [slider setAlpha:1.0];
        }];
    } else {
        [slider setAlpha:1.0];
    }
}

- (void)positionFontResizeSliderForAnnotationItem:(AnnotationItemView *)item animated:(BOOL)animated
{
    if (self.slider.hidden) return;

    CGRect itemFrame = [self convertRect:[item frameIncludingDelButton] fromView:self.annoView];
    UISlider *slider = self.slider;
    CGRect sliderFrame = slider.frame;
    sliderFrame.origin = itemFrame.origin;
    // position left of item
    sliderFrame.origin.x = itemFrame.origin.x - (sliderFrame.size.width + 10.0);
    sliderFrame.origin.y = (itemFrame.origin.y + itemFrame.size.height / 2.0) - (sliderFrame.size.height / 2.0);

    CGRect imageRect = [AnnotationView rectForImage:self.image whenAspectFitToRect:self.annoView.frame];
    CGRect container = [self convertRect:imageRect fromView:self.imageView];
    
    // adjust slider to stay within view
    sliderFrame.origin.x = MAX(sliderFrame.origin.x, container.origin.x);
    sliderFrame.origin.y = MAX(sliderFrame.origin.y, container.origin.y);
    sliderFrame.origin.x = MIN(sliderFrame.origin.x, container.origin.x + container.size.width - sliderFrame.size.width);
    sliderFrame.origin.y = MIN(sliderFrame.origin.y, container.origin.y + container.size.height - sliderFrame.size.height);

    // check if adjusted slider position means it intersects the selected item
    if (CGRectIntersectsRect(sliderFrame, itemFrame)) {
        sliderFrame = slider.frame;
        sliderFrame.origin = itemFrame.origin;
        // position below item
        sliderFrame.origin.x = itemFrame.origin.x;
        sliderFrame.origin.y = itemFrame.origin.y + itemFrame.size.height + 10.0;
        
        // adjust slider to stay within view
        sliderFrame.origin.x = MAX(sliderFrame.origin.x, container.origin.x);
        sliderFrame.origin.y = MAX(sliderFrame.origin.y, container.origin.y);
        sliderFrame.origin.x = MIN(sliderFrame.origin.x, container.origin.x + container.size.width - sliderFrame.size.width);
        sliderFrame.origin.y = MIN(sliderFrame.origin.y, container.origin.y + container.size.height - sliderFrame.size.height);
        
        // check if adjusted slider position means it intersects the selected item
        if (CGRectIntersectsRect(sliderFrame, itemFrame)) {
            sliderFrame = slider.frame;
            sliderFrame.origin = itemFrame.origin;
            // position above item
            sliderFrame.origin.x = itemFrame.origin.x;
            sliderFrame.origin.y = itemFrame.origin.y - sliderFrame.size.height - 10.0;
            
            // adjust slider to stay within view
            sliderFrame.origin.x = MAX(sliderFrame.origin.x, container.origin.x);
            sliderFrame.origin.y = MAX(sliderFrame.origin.y, container.origin.y);
            sliderFrame.origin.x = MIN(sliderFrame.origin.x, container.origin.x + container.size.width - sliderFrame.size.width);
            sliderFrame.origin.y = MIN(sliderFrame.origin.y, container.origin.y + container.size.height - sliderFrame.size.height);
        }
    }
    

    if (animated && (abs(slider.frame.origin.y - sliderFrame.origin.y) >= (sliderFrame.size.height / 2.0))) {
        [UIView animateWithDuration:0.2 animations:^{
            [slider setFrame:sliderFrame];
        }];
    } else {
        [slider setFrame:sliderFrame];
    }
    
    [self bringSubviewToFront:slider];
}

- (void)hideFontResizeSliderAnimated:(BOOL)animated
{
    if (animated) {
        [UIView animateWithDuration:0.15 animations:^{
            [self.slider setAlpha:0.0];
        } completion:^(BOOL finished) {
            [self.slider setHidden:YES];
            [self.slider removeFromSuperview];
        }];
    } else {
        [self.slider setHidden:YES];
        [self.slider removeFromSuperview];
    }
}

#pragma mark - Private methods

- (void)initSetup
{
    self.imageView = [[UIImageView alloc] init];
    [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self addSubview:self.imageView];
    
    ExpandedHitBoundaryView *annoView = self.annoView = [[ExpandedHitBoundaryView alloc] initWithFrame:self.imageView.frame topMargin:MAX_DELETE_BUTTON_HEIGHT right:0.0 bottom:0.0 left:0.0];
    [annoView setClipsToBounds:NO];
    [annoView setOpaque:NO];
//    [annoView setBackgroundColor:[UIColor redColor]];
//    [annoView setAlpha:0.4];
    [self addSubview:annoView];
    [self bringSubviewToFront:annoView];
    [self.slider setHidden:YES];
    [self bringSubviewToFront:self.slider];
    
    self.tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.tapGR setDelegate:self];
    [self addGestureRecognizer:self.tapGR];
    [self addGestureRecognizer:self.longPressGR];
}

- (void)layoutAnnoItems
{
    for (AnnotationItemView *item in self.annoItems) {
        if (![item isDescendantOfView:self.annoView]) {
            BOOL oldValue = self.hasUnsavedChanges;
            [self addAnnotationItem:item atPoint:[self convertPoint:item.frame.origin fromView:self.annoView]];
            self.hasUnsavedChanges = self.hasUnsavedChanges && oldValue;
            [item setBackgroundColor:[UIColor redColor]];
        }
    }
    if (!self.slider.hidden) [self positionFontResizeSliderForAnnotationItem:self.currentAnnoItem animated:NO];
}


- (void)deselectAllAnnotationItems
{
    for (AnnotationItemView *item in self.annoItems) {
        [item setIsSelected:NO];
    }
}

- (void)annotationItemsSetEditMode:(BOOL)editMode
{
    for (AnnotationItemView *item in self.annoItems) {
        [item setEditMode:editMode];
    }
}

- (void)makeAnnotationsDraggable:(BOOL)draggable
{
    if (draggable) {
        [self addGestureRecognizer:self.dragAnnoGR];
    } else {
        [self removeGestureRecognizer:self.dragAnnoGR];
        self.dragAnnoGR = nil;
    }
}

- (AnnotationItemView *)annotationItemContainingPoint:(CGPoint)point
{
    AnnotationItemView *annotationItem = nil;
    for (AnnotationItemView *item in self.annoItems) {
        if ([item isDescendantOfView:self.annoView]) {
            CGRect annoRect = item.frame;
            float padding = DRAG_BORDER_PADDING;
            annoRect.origin.x -= padding;
            annoRect.origin.y -= padding;
            annoRect.size.width += padding * 2.0;
            annoRect.size.height += padding * 2.0;
            if (CGRectContainsPoint(annoRect, point)) {
                annotationItem = item;
            }
        }
    }
    return annotationItem;
}

#pragma mark - Public Class methods

+ (CGRect)rectForImage:(UIImage *)image whenAspectFitToRect:(CGRect)rect
{
    if (!image) return CGRectZero;
    // scale image to fit width
    float scale = 1.0;
    
    scale = rect.size.width / image.size.width;
    
    float w = image.size.width * scale;
    float h = image.size.height * scale;
    
    // scale image to fit height
    if (h > rect.size.height) {
        scale = rect.size.height / h;
        w = w * scale;
        h = h * scale;
    }
    float x = (rect.size.width - w) / 2;
    float y = (rect.size.height - h) / 2;
    
    return CGRectMake(x,y,w,h);
}

#pragma mark - Private Class methods

+ (UIImage *)imageFromView:(UIView *)view useRect:(CGRect)rect scaleFactor:(float)scale
{
    UIImage *img = [AnnotationView imageFromView:view scaleFactor:scale];
    
    UIGraphicsBeginImageContextWithOptions(rect.size, view.opaque, [[UIScreen mainScreen] scale] * scale);
    
    // create rect to draw image in so that area of interst starts at 0,0 point of context
    CGRect drawRect = CGRectMake(-rect.origin.x, -rect.origin.y, img.size.width, img.size.height);
    
    // the part of the image drawn outside of the context gets 'cropped' out!
    [img drawInRect:drawRect];
    
    UIImage* croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return croppedImage;
}

+ (UIImage *)imageFromView:(UIView *)view scaleFactor:(float)scale
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, [[UIScreen mainScreen] scale] * scale);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}


#pragma mark - Private gesture methods

- (void)handleTap:(UIGestureRecognizer *)gr
{
    if ([self.tapGR isEqual:gr]) {
        CGPoint point = [gr locationInView:self];
        if ([gr state] == UIGestureRecognizerStateRecognized) {
            if (!_itemLongStarted) {
                if (self.editMode) {
                    if (self.slider.hidden) {
                        [self hideFontResizeSliderAnimated:YES];
                        AnnotationItemView *prevItem = self.currentAnnoItem;
                        AnnotationItemView *tappedItem = [self annotationItemContainingPoint:[gr locationInView:self.annoView]];
                        self.currentAnnoItem = tappedItem;
                        [tappedItem becomeFirstResponder];
                        if (!tappedItem) [prevItem resignFirstResponder];
                    } else {
                        CGRect itemFrame = [self convertRect:[self.currentAnnoItem frameIncludingDelButton] fromView:self.annoView];
                        if (!CGRectContainsPoint(self.slider.frame, point) && !CGRectContainsPoint(itemFrame, point)) {
                            [self hideFontResizeSliderAnimated:YES];
                        }
                    }
                }
                if ([self.delegate respondsToSelector:@selector(didTapAnnotationView:atPoint:)]) {
                    [self.delegate didTapAnnotationView:self atPoint:point];
                }
            }
        }
    }
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gr
{
    if ([gr isEqual:self.longPressGR]) {
        if (self.editMode) {
            if ([gr state] == UIGestureRecognizerStateBegan) {
                _itemLongStarted = YES;
                // check if longpress is on an item
                AnnotationItemView *item = [self annotationItemContainingPoint:[gr locationInView:self.annoView]];
                if (item) {
                    self.currentAnnoItem = item;
                    [self showFontResizeSliderAnimated:YES];
                }
            } else if ([gr state] == UIGestureRecognizerStateRecognized) {
                _itemLongStarted = NO;
            }
        }
    }
}

-(void)dragging:(UIPanGestureRecognizer *)gesture
{
    CGRect imageRect = self.annoView.bounds;
    AnnotationItemView *item = self.currentAnnoItem;
    if(gesture.state == UIGestureRecognizerStateBegan)
    {
        _panCoord = [gesture locationInView:self.annoView];
        CGPoint touchLocation = _panCoord;
        
        BOOL switchFirstResponder = [self.currentAnnoItem isFirstResponder];
        
        item = [self annotationItemContainingPoint:touchLocation];
        if (item && ![item isEqual:self.currentAnnoItem]) {
            self.currentAnnoItem = item;
            if (switchFirstResponder) [item becomeFirstResponder];
        }

        if (item) {
            [self.annoView bringSubviewToFront:item];
            self.isDraggingAnnotation = YES;
            self.hasUnsavedChanges = YES;
        }
    }
    
    if (gesture.state == UIGestureRecognizerStateCancelled || gesture.state == UIGestureRecognizerStateEnded) {
        self.isDraggingAnnotation = NO;
        // TODO: set item state to normal
    }
    
    if (self.isDraggingAnnotation) {
        CGPoint touchCoord = [gesture locationInView:self.annoView];
        float dX = touchCoord.x - _panCoord.x;
        float dY = touchCoord.y - _panCoord.y;
        
        CGRect newAnnoFrame = CGRectMake(item.frame.origin.x+dX, item.frame.origin.y+dY, item.frame.size.width, item.frame.size.height);
        // restrict annotation item remain within bounds of imageRect
        newAnnoFrame.origin.x = MAX(newAnnoFrame.origin.x, 0.0);
        newAnnoFrame.origin.y = MAX(newAnnoFrame.origin.y, 0.0);
        newAnnoFrame.origin.x = MIN(newAnnoFrame.origin.x, imageRect.size.width - newAnnoFrame.size.width);
        newAnnoFrame.origin.y = MIN(newAnnoFrame.origin.y, imageRect.size.height - newAnnoFrame.size.height);
        item.frame = newAnnoFrame;

        // store touchCoord as _panCoord so it can be used to calculate distance moved while dragging
        _panCoord = touchCoord;
        // avoid drag touch location drifting away from item when dragging outside of imageRect
        _panCoord.x = MAX(_panCoord.x, newAnnoFrame.origin.x - DRAG_BORDER_PADDING);
        _panCoord.y = MAX(_panCoord.y, newAnnoFrame.origin.y - DRAG_BORDER_PADDING);
        _panCoord.x = MIN(_panCoord.x, newAnnoFrame.origin.x + newAnnoFrame.size.width + DRAG_BORDER_PADDING);
        _panCoord.y = MIN(_panCoord.y, newAnnoFrame.origin.y + newAnnoFrame.size.height + DRAG_BORDER_PADDING);

        [self positionFontResizeSliderForAnnotationItem:self.currentAnnoItem animated:YES];
    }
    
}

#pragma mark - UIGestureRecognizerDelegate delegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gr shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGr
{
//    // allow other gesture while dragging (let the owners of those gestures deal with conflicts!
//    if ([gr isEqual:self.dragAnnoGR]) {
//        return YES;
//    }
    return YES;
}

#pragma mark - AnnotationItemViewDelegate methods

- (void)didTapDeleteButtonForAnnotationItemView:(AnnotationItemView *)item
{
    [self removeAnnotationItem:item animated:YES];
    [self hideFontResizeSliderAnimated:YES];
}

#pragma mark - Public IBAction methods

- (IBAction)fontResizeSliderValueChanged:(UISlider *)slider
{
    if ([self.currentAnnoItem respondsToSelector:@selector(setFont:)]) {
        AnnotationLabel *item = (AnnotationLabel *)self.currentAnnoItem;
        [item setFontSize:slider.value];
    }
}


#pragma mark - State Restoration

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super encodeRestorableStateWithCoder:coder];
    
    [coder encodeObject:[NSKeyedArchiver archivedDataWithRootObject:self.annoItems] forKey:@"annoItems"];
    [coder encodeObject:@(self.editMode) forKey:@"editMode"];
    [coder encodeObject:@(self.hasUnsavedChanges) forKey:@"hasUnsavedChanges"];
    [self.annoItems indexOfObject:self.currentAnnoItem];
    [coder encodeObject:[self.currentAnnoItem modelIdentifier] forKey:@"currentAnnoItemModelIdentifier"];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder
{
    [super decodeRestorableStateWithCoder:coder];
    self.editMode = [[coder decodeObjectForKey:@"editMode"] boolValue];
    self.hasUnsavedChanges = [[coder decodeObjectForKey:@"hasUnsavedChanges"] boolValue];
    NSArray *items = (NSArray *)[NSKeyedUnarchiver unarchiveObjectWithData:[coder decodeObjectForKey:@"annoItems"]];
    // TODO: need to ensure z-index is maintained
    self.annoItems = [items mutableCopy];
    NSString *ident = [coder decodeObjectForKey:@"currentAnnoItemModelIdentifier"];
    if (ident) {
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"guid = %@",ident];
        self.currentAnnoItem = [[self.annoItems filteredArrayUsingPredicate:pred] lastObject];
    } else {
        self.currentAnnoItem = nil;
    }
}

@end
