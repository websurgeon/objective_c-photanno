//
//  AnnotationLabel.m
//  PhotAnno
//
//  Created by Pete on 28/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "AnnotationLabel.h"

#define PLACEHOLDER_TEXT @"Start Typing!"
@interface AnnotationLabel ()

@property (strong, nonatomic) UILabel *lbl;

@end

@implementation AnnotationLabel {
    BOOL _hasText;
    float _fontSize;
}

#pragma mark - Public Properties

- (UIFont *)font
{
    return self.lbl.font;
}

- (void)setFont:(UIFont *)font
{
    [self.lbl setFont:font];
    [self resizeToFitLabel];
}

- (float)fontSize
{
    return self.font.pointSize;
}

- (void)setFontSize:(float)fontSize
{
//    UIFontDescriptor *desc = self.font.fontDescriptor;
//    [self setFont:[UIFont fontWithDescriptor:desc size:fontSize]];

    [self setFont:[self.font fontWithSize:fontSize]];
}

- (id)initWithText:(NSString *)text
{
    self = [super init];
    if (self) {
        // Initialization code
        [self initSetupWithText:text withFont:nil];
    }
    return self;
}

- (id)init
{
    return [self initWithText:nil];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.lbl = [aDecoder decodeObjectForKey:@"lbl"];
        _hasText = [[aDecoder decodeObjectForKey:@"hasText"] boolValue];
        if (!self.lbl) {
            [self initSetupWithText:nil withFont:nil];
        }
    }
    return self;
}

- (void)initSetupWithText:(NSString *)text withFont:(UIFont *)font
{
    _hasText = text ? YES : NO;
    UILabel *lbl = [AnnotationLabel makeLabelWithText:(text == nil ? PLACEHOLDER_TEXT : text)];
    if (font) {
        [lbl setFont:font];
    } else {
        [lbl setFont:[self defaultFont]];
    }
    [AnnotationLabel resizeAnnotationLabel:lbl toFitText:lbl.text];
    self.lbl = lbl;
    [self addSubview:lbl];
    [self resizeToFitLabel];
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:@(self.hasText) forKey:@"hasText"];
    [aCoder encodeObject:self.lbl forKey:@"lbl"];
}

- (void)resizeToFitLabel
{
    [AnnotationLabel resizeAnnotationLabel:self.lbl toFitText:self.lbl.text];
    CGRect newFrame = self.frame;
    newFrame.size = self.lbl.frame.size;
    [self setFrame:newFrame];    
}

- (UIFont *)defaultFont
{
    float size = 17.0f;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        size = 34.0f;
    }
    
    return [UIFont systemFontOfSize:size];    
}

+ (void)resizeAnnotationLabel:(UILabel *)label toFitText:(NSString *)string
{
    CGRect newLabelFrame = label.frame;
    NSString *sizeString = [string isEqualToString:@""] ? @"X" : string;
    newLabelFrame.size = [sizeString sizeWithFont:[label font]];
    [label setFrame:newLabelFrame];
}

+ (UILabel *)makeLabelWithText:(NSString *)text
{
    UILabel *item = [[UILabel alloc] init];
    [item setTextColor:[UIColor whiteColor]];
    [item setBackgroundColor:[UIColor blackColor]];
    [item sizeToFit];
    [item setAdjustsFontSizeToFitWidth:YES];
    [item setText:text];
    return item;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // TODO: need to reconsider how the label font resizing works when orientation changes it does not currently maintain the correct ratio to the image size 
    CGRect frame = self.lbl.frame;
    frame.size = self.frame.size;
    [self.lbl setFrame:frame];
}

#pragma mark - UIKeyInput methods

- (BOOL)hasText
{
    return _hasText;
}

- (void)insertText:(NSString *)text
{
    if (!self.hasText) {
        self.lbl.text = @"";
        _hasText = YES;
    }
    NSString *newText = [self.lbl.text stringByAppendingString:text];
    [self.lbl setText:newText];
    [self resizeToFitLabel];
}

- (void)deleteBackward
{
    if (self.hasText) {
        [self.lbl setText:[self.lbl.text substringToIndex:[self.lbl.text length] - 1]];
        if ([self.lbl.text isEqualToString:@""]) {
            [self.lbl setText:PLACEHOLDER_TEXT];
            _hasText = NO;
        }
        [self resizeToFitLabel];
    }    
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (float)fontMaxSize
{
    return [self defaultFont].pointSize * 5;
}

- (float)fontMinSize
{
    return [self defaultFont].pointSize * 0.25;
}

@end
