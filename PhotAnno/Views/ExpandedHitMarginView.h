//
//  ExpandedHitMarginView.h
//  PhotAnno
//
//  Created by Pete on 31/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//
//  Allows the view to handle touches that are outside of its bounds by the supplied margin values

#import <UIKit/UIKit.h>

@interface ExpandedHitBoundaryView : UIView

@property (nonatomic) float top;
@property (nonatomic) float right;
@property (nonatomic) float bottom;
@property (nonatomic) float left;

- (id)initWithFrame:(CGRect)frame topMargin:(float)top right:(float)right bottom:(float)bottom left:(float)left;

@end
