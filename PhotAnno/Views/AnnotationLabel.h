//
//  AnnotationLabel.h
//  PhotAnno
//
//  Created by Pete on 28/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "AnnotationItemView.h"

@interface AnnotationLabel : AnnotationItemView <UIKeyInput, NSCoding>

@property (strong, nonatomic) UIFont *font;
@property (nonatomic) float fontSize;

- (id)initWithText:(NSString *)text;
- (id)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)aCoder;
- (UIFont *)defaultFont;

- (float)fontMaxSize;
- (float)fontMinSize;

@end
