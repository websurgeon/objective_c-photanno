//
//  ImageSourceVCTests.m
//  PhotAnnoTests
//
//  Created by Pete on 26/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "ImageSourceVC.h"

#import <SenTestingKit/SenTestingKit.h>

#define HC_SHORTHAND
#import <OCHamcrestIOS/OCHamcrestIOS.h>

#define MOCKITO_SHORTHAND
#import <OCMockitoIOS/OCMockitoIOS.h>

@interface ImageSourceVCTests : SenTestCase
@end

@implementation ImageSourceVCTests

- (void)setUp
{
    [super setUp];

    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testImagePickerDefault
{
    ImageSourceVC *sut = [[ImageSourceVC alloc] init];
    assertThat([sut imagePickerController], notNilValue());
}

- (void)testShowImagePickerSetsCorrectSourceType
{
// TODO: this test currently causes error when run for iPad
    /*
     UIPopoverController presentPopoverFromRect:inView:permittedArrowDirections:animated:]: Popovers cannot be presented from a view which does not have a window.
     */

//    ImageSourceVC *sut = [[ImageSourceVC alloc] init];
//    UIImagePickerController *mockPicker = mock([UIImagePickerController class]);
//
//    [sut setImagePickerController:mockPicker];
//    [sut view];
//    [sut showImagePicker:nil];
//    [verify(mockPicker) setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (void)testShowImagePickerPresentsPickerForIphone
{
//    ImageSourceVC *sut = [[ImageSourceVC alloc] init];
//    [sut view];
//    [sut showImagePicker:nil];
    
/*
 Warning: Attempt to present <UIImagePickerController: 0xff97a00> on <ImageSourceVC: 0xff97450> whose view is not in the window hierarchy!
*/

// TODO: research how to test that image picker is presented
    

//    assertThat([sut presentingViewController], equalTo(sut.imagePickerController));
 
}

- (void)testShowImagePickerPopoverForIpad
{
//    ImageSourceVC *sut = [[ImageSourceVC alloc] init];
//    [sut view];
//    [sut showImagePicker:nil];
//    UIPopoverController *mockPopover = mock([UIPopoverController class]);
//    [verify(mockPopover) didAddSubview:sut.imagePickerController.view];
    
// TODO: research how to test that image picker is shown in popover
/*
 UIPopoverController presentPopoverFromRect:inView:permittedArrowDirections:animated:]: Popovers cannot be presented from a view which does not have a window.
 */
}




@end
