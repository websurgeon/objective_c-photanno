//
//  MainStoryboard_iPhoneTests.m
//  PhotAnnoTests
//
//  Created by Pete on 26/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

#define HC_SHORTHAND
#import <OCHamcrestIOS/OCHamcrestIOS.h>

#import "ImageSourceVC+Testing.h"

#define SBNAME @"MainStoryboard_iPad"
#define IDENTIFIER_IMAGESOURCEVC @"ImageSourceVC"

@interface MainSB_iPadTests : SenTestCase
@end

@implementation MainSB_iPadTests {
    UIStoryboard *storyboard;
    UINavigationController *initialNavController;
}

- (void)setUp
{
    [super setUp];
    
    storyboard = [UIStoryboard storyboardWithName:SBNAME bundle:nil];
    initialNavController = [storyboard instantiateInitialViewController];
}

- (void)tearDown
{
    storyboard = nil;
    initialNavController = nil;
    
    [super tearDown];
}

- (void)testInitialVCIsNavController
{    
    assertThatBool([initialNavController isKindOfClass:[UINavigationController class]], equalToBool(YES));
}

- (void)testNavControllerRootVC
{    
    UIViewController *vc = [initialNavController topViewController];
    
    assertThatBool([vc isKindOfClass:[ImageSourceVC class]], equalToBool(YES));
}

#pragma mark - ImageSourceVC Tests

- (void)test_ImageSourceVC_BackgroundImageViewConnected
{    
    ImageSourceVC *sut = [storyboard instantiateViewControllerWithIdentifier:IDENTIFIER_IMAGESOURCEVC];
    [sut view];
    assertThat([sut backgroundImageView], notNilValue());
}

- (void)test_ImageSourceVC_PickImageButtonConnected
{
    ImageSourceVC *sut = [storyboard instantiateViewControllerWithIdentifier:IDENTIFIER_IMAGESOURCEVC];
    [sut view];
    assertThat([sut btn_pickImage], notNilValue());
}

- (void)test_ImageSourceVC_PickImageButtonAction
{
    ImageSourceVC *sut = [storyboard instantiateViewControllerWithIdentifier:IDENTIFIER_IMAGESOURCEVC];
    [sut view];
    assertThat([[sut btn_pickImage] actionsForTarget:sut forControlEvent:UIControlEventTouchUpInside], contains(@"showImagePicker:", nil));
}

- (void)test_ImageSourceVC_TakePhotoButtonConnected
{
    ImageSourceVC *sut = [storyboard instantiateViewControllerWithIdentifier:IDENTIFIER_IMAGESOURCEVC];
    [sut view];
    assertThat([sut btn_takePhoto], notNilValue());
}

- (void)test_ImageSourceVC_TakePhotoButtonAction
{
    ImageSourceVC *sut = [storyboard instantiateViewControllerWithIdentifier:IDENTIFIER_IMAGESOURCEVC];
    [sut view];
    assertThat([[sut btn_takePhoto] actionsForTarget:sut forControlEvent:UIControlEventTouchUpInside], contains(@"launchCamera:", nil));
}

- (void)testSegueWhenImagePicked
{
    SEL realPrepareForSegue = @selector(prepareForSegue:sender:);
    SEL testPrepareForSegue = @selector(tests_prepareForSegue:sender:);
    
    ImageSourceVC *sut = (ImageSourceVC *)[initialNavController topViewController];
    [sut view];
    
    // swap out prepareForSegue:sender: with a test version of the method that will allow the segue and sender to be stored
    [ImageSourceVC swapInstanceMethodsForClass:[ImageSourceVC class]
                                      selector:realPrepareForSegue
                                   andSelector:testPrepareForSegue];

    [sut imagePicked:nil withURL:nil andMetaData:nil];
    
    // get segue object that is stored when Tests_prepareForSegue:sender: is called
    UIStoryboardSegue *segue = objc_getAssociatedObject(sut, ImageSourceVC_storyboardSegueKey);
    id sender = objc_getAssociatedObject(sut, ImageSourceVC_senderKey);
    objc_removeAssociatedObjects(sut);
    
    // check segue is the expected type
    assertThat(NSStringFromClass([segue class]), equalTo(UIStoryboardPushSegue));
    
    // check destination is the expected class
    assertThatBool([[segue destinationViewController] isKindOfClass:[EditorVC class]], equalToBool(TRUE));
    
    // check sender is the vc that called the action
    assertThat(sender, equalTo(sut.imagePickerController));
    
    // swap out prepareForSegue:sender: with a test version of the method that will allow the segue and sender to be stored
    [ImageSourceVC swapInstanceMethodsForClass:[ImageSourceVC class]
                                      selector:realPrepareForSegue
                                   andSelector:testPrepareForSegue];
}

@end
