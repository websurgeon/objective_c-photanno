//
//  ImageSourceVC+Testing.h
//  PhotAnno
//
//  Created by Pete on 26/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import <objc/runtime.h>
#import "ImageSourceVC.h"
#import "EditorVC.h"

#define UIStoryboardPushSegue @"UIStoryboardPushSegue" // class name for hidden subclass of UIStoryboardSegue

// keys used for storing segue and sender passed into tests_prepareForSegue:sender:
static const char *ImageSourceVC_storyboardSegueKey = "ImageSourceVCTestAssociatedStoryboardKey";
static const char *ImageSourceVC_senderKey = "ImageSourceVCTestAssociatedSenderKey";

@interface ImageSourceVC (Testing)

- (void)tests_prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

+ (void)swapInstanceMethodsForClass:(Class)cls selector:(SEL)sel1 andSelector:(SEL)sel2;

@end
