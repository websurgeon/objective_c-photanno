//
//  ImageSourceVC+Testing.m
//  PhotAnno
//
//  Created by Pete on 26/10/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "ImageSourceVC+Testing.h"

@implementation ImageSourceVC (Testing)

// this category method is used to swap in place the classes real prepareForSegue:sender: method
- (void)tests_prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    objc_setAssociatedObject(self, ImageSourceVC_storyboardSegueKey, segue, OBJC_ASSOCIATION_RETAIN);
    objc_setAssociatedObject(self, ImageSourceVC_senderKey, sender, OBJC_ASSOCIATION_RETAIN);
}

#pragma mark - Helper Methods

+ (void)swapInstanceMethodsForClass:(Class)cls selector:(SEL)sel1 andSelector:(SEL)sel2 {
    Method method1 = class_getInstanceMethod(cls, sel1);
    Method method2 = class_getInstanceMethod(cls, sel2);
    method_exchangeImplementations(method1, method2);
}

@end