# PhotAnno

The goal of this project is to provide a very simple way to annotate photos.

This project is currently in development (v0.1) and will not be release to the App Store until v1.0

*** This project has been put on 'hold' (indefinitely) as I currently do not wish to devote any further time on this (there have been no commits since November 2013 anyway!). I have also learned a lot since last working on this project and so, if I were to continue this project, I would want to take a fresh look at how this should be implemented. ***

## Road Map

The initial features of this app have been kept deliberatly sparse to ensure a short delivery time for v1.0.

v1.0 features are:  
- Basic text annotation functionality Only  
- Saving annotated photo "Anno" (so they can be shared like other photos)  
- Take photo from within app (instead of picking from existing)  
- Move / Delete annotations


Once these basics have been acheived there will be plenty of scope for additional features to make the app much more useful.


## Possible Future Features:  
- Additional control over editing annotations  
- Choice of font, font size, color (font and background)  
- Pan and Zoom photos  
- Share from within app  
- Annotation shapes (arrows, rectangles, circles)  
- Free form (finger drawing) annotation  
- Edit photo meta data (time, created date, geo tag...)  
- Add QR code to image (that encodes a URL entered by the user)  